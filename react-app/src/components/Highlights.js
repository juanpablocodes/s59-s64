import { Row, Col, Card } from 'react-bootstrap';


export default function Highlights() {
	return (

		<Row className="mt-3 mb-3">
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				     <Card.Body>
				       <Card.Title>Bulls Cap</Card.Title>
				       <Card.Text>
				       Sample Text
				       </Card.Text>
				     </Card.Body>
				</Card>
			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				     <Card.Body>
				       <Card.Title>Raider Cap</Card.Title>
				       <Card.Text>
				        Sample Text
				       </Card.Text>
				     </Card.Body>
				</Card>

			</Col>

			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
				     <Card.Body>
				       <Card.Title>LA Cap</Card.Title>
				       <Card.Text>
				         Sample Textd
				       </Card.Text>
				     </Card.Body>
				</Card>

			</Col>
		</Row>
			
	)
};