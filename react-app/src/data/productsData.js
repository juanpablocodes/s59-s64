const productsData = [

	{
		id: "wdc001",
		name: "Bulls Cap",
		description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
		price: 4500,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Raiders Cap",
		description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
		price: 5000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "LA Cap",
		description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
		price: 5500,
		onOffer: true
	}

]

export default productsData;