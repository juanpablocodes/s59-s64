import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
// import Courses from '../components/CourseCard';
// import Resources from '../components/Resources'

export default function Home() {

    const data = {
        title: "Sample E-Commerce",
        content: "This is just a sample",
        destination: "/products",
        label: "Order Now!"
    }

    return (
        <Fragment

        >
            <Banner data={data}/>
            <Highlights/>
             {/*<CourseCard/>*/}
            {/*<Resources/>*/}
        </Fragment>
    )
};
