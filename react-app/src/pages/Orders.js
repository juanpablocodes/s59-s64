import { Fragment, useEffect, useState, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';

export default function Orders() {

	const [orders, setOrders] = useState([]);

	const {user} = useContext(UserContext);

	useEffect(() => {
		fetch('http://localhost:3000/users/detail')
		.then(res => res.json())
		.then(data => {

			if(typeof data._id !== "undefined") {
            setOrders({
                id: data._id,
                isAdmin: data.isAdmin
            })
          } else { // user is logged out

            setOrders({
              id: null,
              isAdmin: null
            })

          }
      })

  }, []);

	return (
		(user.isAdmin)
		?
			<Navigate to="/orders"/>
		:
			<Fragment>
				<h1 className="text-center my-3">Products</h1>
				{orders}
			</Fragment>
	)
}

