const express = require('express');
const router = express.Router();
const auth = require('../auth');
const orderController = require('../controllers/orderController');

// Route to add an order
router.post('/', auth.verify, (req,res) => {

    const userData = auth.decode(req.headers.authorization)

    orderController.addOrder(req.body, {userId: userData.id, isAdmin: userData.isAdmin}).then(resultFromController => 
        res.send(resultFromController))
});

// Stretch Goals: Route to retrieve all orders
router.get('/allorders', auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)

    orderController.allOrders(userData).then(resultFromController => res.send(resultFromController))
});


module.exports = router
