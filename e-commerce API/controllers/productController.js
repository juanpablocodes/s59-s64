const Product = require("../models/Product.js");
const User = require("../models/User.js");

// Create a new product
module.exports.addProduct = (reqBody, userData) => {

	return User.findById(userData.userId).then((result) => {
		if(userData.isAdmin == false) {
			return "You are not an Admin!";
		} else {
			let newProduct = new Product({
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			});
			return newProduct.save().then((Product, error) => {
				if(error) {
					return false;
				} else {
					return "Product successfully added!"
				}
			})
		}
	})
};

// Controller function to retrieve all active products
module.exports.getAllActive = () => {
	return Product.find({isActive: true}). then(result => {
		return result;
	})
};

// module.exports.getAllProducts = (data) => {
//     if (data.isAdmin) {
//         return Product.find({}).then(result => {

//         return result
//     })
// } else {
//     return false
// }


// Controller function to retrieve a specific product
module.exports.getSpecificProduct = (reqParams) => {
	return Product.findById(reqParams.productId). then(result => {
		return result;
	})
};

// Controller function to update a product
module.exports.updateProduct = (reqParams, reqBody, data) => {
	
	if(data) {
		let updatedProduct = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		};

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((updatedProduct, error) => {
			if(error) {
				return false;
			} else {
				return 'Product details successfully updated!';
			}
		})
	} else {
		return 'You are not an Admin!';
	}
};

// Controller function to archive a product
module.exports.archiveProduct = (data) => {

	return Product.findById(data.productId).then((result, error) => {

		if(data.isAdmin === true) {
			result.isActive = false;

			return result.save().then((archivedProduct, error) => {
				if(error) {
					return false;
				} else {
					return 'Product successfully archived!';
				}
			})
		} else {
			return 'You are not an Admin!';
		}
	})
};

