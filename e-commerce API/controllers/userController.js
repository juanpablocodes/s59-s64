const User = require("../models/User.js");
const Product = require("../models/Product.js");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Register
module.exports.registerUser = async (reqBody) => {
    
    let checkEmailExists = await User.find({
        email: reqBody.email
    })
    if (checkEmailExists.length) {
        return "Email already exists!"
    } else {
    let newUser = new User({
        fullName: reqBody.fullName,
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10)
    })
    return await newUser.save().then((User, error) =>{
        if(error){
            return false
        } else {
            return "User Successfully Registered!"
        }
    })
}
};

// Login
module.exports.loginUser = (reqBody) => {
    return User.findOne({
        email: reqBody.email  
    })
    .then(result => {
        if (result == null){
            return false
        } else{
            
            const checkPassword = bcrypt.compareSync(reqBody.password, result.password)
            if(checkPassword){
                return {access: auth.createAccessToken(result)}
            } else{
                return false
            }
        }
    })
};

// Retrieve User Details
module.exports.getProfile = (userData) => {
    return User.findById(userData.userId).then(result => {
console.log(userData)
console.log(result)
        if(result == null) {
            return false
        } else {
             result.password = "******";
            return result
        }
    })
};

// Stretch Goals: Set User to Admin
module.exports.setUserToAdmin = async (reqBody, userData) => {

    if(userData.isAdmin) {
        let updatedAdmin = {
            userId: reqBody.id,
            isAdmin: reqBody.isAdmin
        }

        return await User.findByIdAndUpdate(reqBody.userId, updatedAdmin).then((updatedAdmin, error) => {
            if(error) {
                return false
            } else {
                return "User successfully updated as an admin!"
            }
        })
    }
};