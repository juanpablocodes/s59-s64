const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		require: [true, "Product Name is required"]
	},
	description: {
		type: String,
		require: [true, "Product description is required"]
	},
	price: {
		type: Number,
		default: 0
	},
	isActive: {
		type: Boolean,
		default: true,
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	// userOrders: [
	// 	{
	// 		orderId: {
	// 			type: String,
	// 			require: [true, "Order Id is required"]
	// 		}
	// 	}

	// ]
});

module.exports = mongoose.model("Product", productSchema);
